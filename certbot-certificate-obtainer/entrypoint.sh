#!/bin/sh

# Prevent data loss
# on terminating.
trap exit TERM

echo "--> Generate Diffie-Hellman 2048 len keys if it's not exists."
mkdir -p /etc/letsencrypt/certs
DHPARAM_FILE=/etc/letsencrypt/certs/dhparam.pem
if [ ! -f "$DHPARAM_FILE" ]; then
  openssl dhparam -out $DHPARAM_FILE 2048
fi

echo "--> Obtain certificate if it's not exists."
if [[ "${ENVIRONMENT}" == 'production' ]]; then
  certbot certonly \
    --non-interactive \
    --keep-until-expiring \
    --webroot \
    --webroot-path=/var/www/html \
    --email ${CERTBOT_EMAIL} \
    --agree-tos \
    --no-eff-email \
    -d "${SERVER_ADDRESS}"
else
  certbot certonly \
    --non-interactive \
    --keep-until-expiring \
    --webroot \
    --webroot-path=/var/www/html \
    --email ${CERTBOT_EMAIL} \
    --agree-tos \
    --no-eff-email \
    --staging \
    -d "${SERVER_ADDRESS}"
fi

echo "--> Renew certificate if needed every 12h."
while :; do
  certbot renew
  sleep 12h & wait ${!}
done
