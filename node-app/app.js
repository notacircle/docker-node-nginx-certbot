const express = require('express')
const app = express()
const port = 8080

app.get('/', (req, res) => res.send('node-app is online!'))
app.listen(port, () => console.log(`Listen at port: ${port}.`))
