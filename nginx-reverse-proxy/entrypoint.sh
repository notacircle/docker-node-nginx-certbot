#!/bin/sh

echo "--> Clear nginx conf directory."
rm -f /etc/nginx/conf.d/*

echo "--> Substitute envs in nginx.conf."
envsubst "`env | awk -F = '{printf \" \\\\$%s\", $1}'`" \
 < ./conf.d/nginx.tmpl > /etc/nginx/conf.d/nginx.conf

# log conf file
cat /etc/nginx/conf.d/nginx.conf

echo "--> Start nginx daemon and reload certificates every 6h."
while :; do
  sleep 6h & wait ${!}
  nginx -s reload
done & nginx -g "daemon off;"
